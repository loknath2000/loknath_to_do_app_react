import React, { useState } from 'react';

function Todos({todoList,deleteHandler}) {
  const [complete , setComplete] = useState(false);
   const [edit, setEdit] = useState(false);

  const Completed = () =>{
    !complete ? setComplete(true) :setComplete(false);
  }
  const editHandler =()=>{
    !edit ? setEdit(true) : setEdit(false);
  }


  return (
    <div className="todo-app">
      {todoList.map((todo,index)=>
        <div key = {index} className="data">
        <h5 contentEditable = {edit? true : false} style={{ textDecoration: complete ? 'line-through' : 'none' ,background: edit ? "white" : 'none'}} id = {index}>{todo}</h5>
        <input type="checkbox" name ="checkbox" onClick={Completed}/>
        <button>Completed</button>
        {edit ? <button  onClick={editHandler}>SAVE</button> : <button onClick={editHandler}>EDIT</button>}
        <button onClick={()=>deleteHandler(index)}>Delete</button>
        </div>
      )}
    </div>
  )
}

export default Todos;
