
import React,{useState,useEffect} from 'react';
import './App.css';
import Todos from './components/Todos';


function App() {
  const [task,setTask]=useState("");
  const [todos, setTodos]=useState([]);
 

  const changeHandler = (e)=>{
    setTask(e.target.value)
  }
  const submitHandler = (e)=>{
    e.preventDefault();
    const newTodos = [...todos,task];
    setTodos(newTodos);
    setTask("");

    for (let index= 0 ; index<newTodos.length ;index++){
    localStorage.setItem(index, newTodos[index]);
    }
  }

  
  
  const deleteHandler = (indexValue) =>{
    
    const newTodos = todos.filter((todo,index)=> index!== indexValue);
    setTodos(newTodos)
    
    for(let index = 0 ;index<todos.length;index++){
      localStorage.removeItem(index, todos[index]);
    }
    
    for (let index= 0 ; index<newTodos.length ;index++){
      localStorage.setItem(index, newTodos[index]);
    }
  }
  
  useEffect(() => {

   for (let i = 0; i < localStorage.length; i++) {

     setTodos((oldTodos) => {
       let newTodos = [...oldTodos, localStorage.getItem(localStorage.key(i))]
       return newTodos
     })
   }
 }, [])
  


  return (
    <div className="App">
          <h1 className='card-title'>Todo list</h1>
          <form onSubmit={submitHandler}>
            <input type="text" name='task' value={task} onChange={changeHandler}/>
            <input type="submit" value="Add" name="Add"/>
          </form>
          <Todos todoList={todos} deleteHandler={deleteHandler}/>
        </div>
  );
}

export default App;
